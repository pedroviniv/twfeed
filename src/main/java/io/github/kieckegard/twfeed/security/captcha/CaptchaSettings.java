/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.security.captcha;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@Component
@ConfigurationProperties(prefix = "google.recaptcha.key")
public class CaptchaSettings {
    
    private String site;
    private String secret;

    public CaptchaSettings(String site, String secret) {
        this.site = site;
        this.secret = secret;
    }

    public CaptchaSettings() {
    }

    public String getSite() {
        return site;
    }

    public String getSecret() {
        return secret;
    }

    @Override
    public String toString() {
        return "CaptchaSettings{" + "site=" + site + ", secret=" + secret + '}';
    }
}
