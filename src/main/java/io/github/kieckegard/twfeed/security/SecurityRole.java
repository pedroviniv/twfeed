/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.security;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public enum SecurityRole {

    ADMIN, USER;

}
