/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.security.captcha;

import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

public class ReCaptchaAuthenticationFilter extends OncePerRequestFilter {

    private final String CAPTCHA_CHALLENGE_FIELD = "recaptcha_challenge_field";
    private final String CAPTCHA_RESPONSE_FIELD = "recaptcha_response_field";

    
    private CaptchaService captchaService;
    
    @Autowired
    public ReCaptchaAuthenticationFilter(CaptchaService captchaService) {
        this.captchaService = captchaService;
    }

    private static final Logger LOG = Logger.getLogger(ReCaptchaAuthenticationFilter.class.getName());

    
    public boolean isAuthorized() {
        SecurityContext ctx = SecurityContextHolder.getContext();
        if(ctx == null)
            return false;
        
        Authentication authentication = ctx.getAuthentication();
        
        return !(authentication == null || !authentication.isAuthenticated());
    }
    
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain fc) throws ServletException, IOException {

        
        
        if (!request.getMethod().equalsIgnoreCase("POST")) {
            fc.doFilter(request, response);
            return;
        }
        
        if(request.getMethod().equalsIgnoreCase("POST") && isAuthorized()) {
            fc.doFilter(request, response);
            return;
        }
        
        String gRecaptchaResponse = request.getParameter("g-recaptcha-response");

        if (gRecaptchaResponse == null || gRecaptchaResponse.isEmpty()) {
            response.sendRedirect("/login?invalidCaptcha");
            return;
        }

        try {
            this.captchaService.processResponse(gRecaptchaResponse);
            fc.doFilter(request, response);
        } catch (ReCaptchaInvalidException ex) {
            response.sendRedirect("/login?invalidCaptcha");
        } catch (ReCaptchaUnavailableException ex1) {
            response.sendRedirect("/login?unavailableCaptcha");
        }

    }

}
