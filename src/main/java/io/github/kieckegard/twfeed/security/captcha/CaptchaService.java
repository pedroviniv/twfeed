/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.security.captcha;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public interface CaptchaService {

    String getReCaptchaSecret();

    String getReCaptchaSite();

    void processResponse(final String response);
    
}
