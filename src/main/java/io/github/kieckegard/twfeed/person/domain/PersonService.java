/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.person.domain;

import io.github.kieckegard.twfeed.person.infraestructure.PersonRepository;
import javax.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public void addPerson(Person person) {

        if (!personRepository.alreadyExists(person.getEmail())) {
            person.setPassword(passwordEncoder.encode(person.getPassword()));
            personRepository.save(person);
        } else {
            throw new ValidationException(ErrorMessages.ALREADY_REGISTERED.toErrorMsg());
        }

    }

}
