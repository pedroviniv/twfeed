/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.person.view;

import io.github.kieckegard.twfeed.person.domain.Person;
import io.github.kieckegard.twfeed.person.domain.PersonService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@Controller
public class PersonController {

    @Autowired
    private PersonService personService;
    @Autowired
    protected AuthenticationManager authenticationManager;

    @PostMapping(value = "/registrar")
    @ResponseBody()
    public String addPerson(Person person, HttpServletResponse response, HttpServletRequest request) {
        try {
            personService.addPerson(person);

            request.login(person.getEmail(), person.getPassword());
            response.setStatus(HttpStatus.CREATED.value());

        } catch (ValidationException ve) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return ve.getMessage();
        } catch (ServletException | UnsupportedOperationException ex) {
            response.setStatus(HttpStatus.CREATED.value());
        }

        return "/";
    }

    @RequestMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "/login";
    }

}
