/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.person.infraestructure;

import io.github.kieckegard.twfeed.person.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    Person findByName(String personName);

    Person findByEmail(String email);

    @Query("SELECT CASE WHEN (COUNT(*) > 0) THEN TRUE ELSE FALSE END FROM Person p WHERE p.email = :email")
    public boolean alreadyExists(@Param("email") String email);

}
