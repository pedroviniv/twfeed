/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.tweet.domain;

import io.github.kieckegard.twfeed.posts.domain.Post;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public interface TweetService {
    
    String tweet(Post post);
}
