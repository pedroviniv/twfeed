/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.tweet.client.data;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class TweetResponse extends Response {
    
    private String tweet;

    public TweetResponse(String tweet, boolean success, String message, int message_code) {
        super(success, message, message_code);
        this.tweet = tweet;
    }

    protected TweetResponse() {}

    public String getTweet() {
        return tweet;
    }

    public void setTweet(String tweet) {
        this.tweet = tweet;
    }

    @Override
    public String toString() {
        return "TweetResponse{" + "tweet=" + tweet + '}';
    }    
}
