/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.tweet.client.data;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class MediaResponse extends Response {
    
    private String media;

    public MediaResponse(String media, boolean success, String message, int message_code) {
        super(success, message, message_code);
        this.media = media;
    }

    protected MediaResponse() {
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    @Override
    public String toString() {
        return "MediaResponse{" + "media=" + media + '}';
    }
}
