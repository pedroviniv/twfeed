/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.tweet.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * Model representing the tweetClient configuration.
 * It includes base_uri (the base_uri of the service)
 * and the uri of the REST resources like: tweets and media.
 * 
 * This model injects the property tweet.service.baseUri (in the application.properties file)
 * through the Spring @Value annotation.
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Component
public class TweetClientConfiguration {
   
    @Value("tweet.service.baseUri")
    private String baseUri;

    public TweetClientConfiguration(String baseUri) {
        this.baseUri = baseUri;
    }
    
    protected TweetClientConfiguration() {}
    
    public static class Resources {

        public static final String TWEET = "tweets";
        public static final String MEDIA = "media";

    }
    
    public String getTweetEndpoint() {
        return this.baseUri + "/" + Resources.TWEET;
    }
    
    public String getMediaEndpoint() {
        return this.baseUri + "/" + Resources.MEDIA;
    }
    
    public String getBaseUri() {
        return this.baseUri;
    }

    public void setBaseUri(String baseUri) {
        this.baseUri = baseUri;
    }

    @Override
    public String toString() {
        return "TweetClientConfiguration{" + "baseUri=" + baseUri + '}';
    }
}
