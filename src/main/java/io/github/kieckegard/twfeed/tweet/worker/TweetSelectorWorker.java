/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.tweet.worker;

import io.github.kieckegard.twfeed.posts.domain.Post;
import io.github.kieckegard.twfeed.posts.infrastructure.PostRepository;
import io.github.kieckegard.twfeed.publication.domain.PublicationService;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 * 
 * Schedule that publishes two posts in each hour between 6 a.m and 23 p.m.
 * these two posts are published on a random minute/second.
 */
@Component
public class TweetSelectorWorker {
    
    private PostRepository postRepository;
    private PublicationService publicationService;
    private static final Logger LOG = Logger.getLogger(TweetSelectorWorker.class.getName());

    @Autowired
    public TweetSelectorWorker(PostRepository postRepository, PublicationService publicationService) {
        this.postRepository = postRepository;
        this.publicationService = publicationService;
    }
    
    /**
     * Randomizes a time based on the hour of the time passed by parameter.
     * An minute/second pair between 1 and 59 is randomized and a LocalTime
     * is created using the hour and the randomized minute/seconds.
     * @param time time contaning the hour
     * @return instance of LocalTime representing the randomized time
     */
    private LocalTime randomTimeInTime(LocalTime time) {
        
        int one = 1;
        int limit = 59;
        
        Random random = new Random();
        int minutes = random.nextInt(limit - one) + one;
        int seconds = random.nextInt(limit - one) + one;
        
        return LocalTime.of(time.getHour(), minutes, seconds);
    }
    
    //would be nice to make the cron configurable via the property file
    //the code below will be refactored
    @Scheduled(cron = "0 0 6-23 * * *")
    public void selectPosts() {
        
        LocalTime now = LocalTime.now();
        List<Post> unpublishedPosts = this.postRepository.getUnpublishedPostsWithLimit(34);
        
        int size = unpublishedPosts.size();
        
        if(size == 0)
            return;
        
        if(size < 2) {
            
            Post firstPost = unpublishedPosts.get(0);
            
            LocalTime firstTweetTaskTime = randomTimeInTime(now);
            LOG.log(Level.INFO, "First tweet will be published at: {0}", firstTweetTaskTime);
            long firstTweetDelay = ChronoUnit.MILLIS.between(now, firstTweetTaskTime);
            ScheduledExecutorService firstTweetScheduler = Executors.newScheduledThreadPool(1);
            firstTweetScheduler.schedule(new Publisher(this.publicationService, firstPost), 
                    firstTweetDelay, TimeUnit.MILLISECONDS);
            
            return;
        }
        
        
        LocalTime secondTweetTaskTime = randomTimeInTime(now);
        LOG.log(Level.INFO, "Second tweet will be published at: {0}", secondTweetTaskTime);
        long secondTweetDelay = ChronoUnit.MILLIS.between(now, secondTweetTaskTime);
        ScheduledExecutorService secondTweetScheduler = Executors.newScheduledThreadPool(1);
        secondTweetScheduler.schedule(new Publisher(this.publicationService, null), 
                secondTweetDelay, TimeUnit.MILLISECONDS);
        
    }
}
