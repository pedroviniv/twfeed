/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.tweet.client.data;

import java.io.Serializable;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class Tweet implements Serializable {
    
    private String text;
    private String media;
    private String source_url;

    public Tweet(String text, String media, String source_url) {
        this.text = text;
        this.media = media;
        this.source_url = source_url;
    }

    protected Tweet() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getSource_url() {
        return source_url;
    }

    public void setSource_url(String source_url) {
        this.source_url = source_url;
    }

    @Override
    public String toString() {
        return "Tweet{" + "text=" + text + ", media=" + media + ", source_url=" + source_url + '}';
    }
}
