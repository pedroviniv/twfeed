/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.tweet.client;

import io.github.kieckegard.twfeed.tweet.client.data.MediaResponse;
import io.github.kieckegard.twfeed.tweet.client.data.TweetResponse;
import io.github.kieckegard.twfeed.tweet.client.data.Tweet;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.kieckegard.twfeed.tweet.domain.MockedTweetService;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * A TweetClient implementation using Spring RestTemplates 
 * and jackson ObjectMapper to achieve the HTTP requests.
 * 
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@Component
public class TweetClient {
    private static final Logger LOG = Logger.getLogger(MockedTweetService.class.getName());
    
    private TweetClientConfiguration clientConfiguration;
    private RestTemplate restTemplate;
    private ObjectMapper objectMapper;

    @Autowired
    public TweetClient(TweetClientConfiguration clientConfiguration, RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.clientConfiguration = clientConfiguration;
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    private Resource createImageResource(String url) throws URISyntaxException, MalformedURLException {

        URI uri = new URI(url);
        Resource resource = new UrlResource(uri);

        return resource;
    }
    
    /**
     * Uploads an image in the twitter.
     * @param mediaURL the url of the image
     * @return an instance of the MediaResponse containg the upload response
     */
    public MediaResponse uploadMedia(String mediaURL) {
        
        String mediaURI = this.clientConfiguration.getMediaEndpoint();
        
        try {
            
            Resource resource = this.createImageResource(mediaURL);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.add("media", resource);
            
            HttpEntity<MultiValueMap<String, Object>> requestEntity 
                    = new HttpEntity<>(body, headers);
            
            ResponseEntity<String> responseEntity = this.restTemplate
                    .postForEntity(mediaURI, 
                            requestEntity, String.class);
            
            String jsonResponseEntity = responseEntity.getBody();
            
            return objectMapper.readValue(jsonResponseEntity, MediaResponse.class);

        } catch (URISyntaxException | MalformedURLException ex) {
            throw new UnavailableTweetServiceException(
                mediaURI + " is not a valid address", ex
            );
        } catch (IOException ex) {
            throw new UnavailableTweetServiceException("Coudln't parse response to "
                    + MediaResponse.class.getName(), ex);
        }
        
    }

    /**
     * Tweets the tweet in the Twitter
     * @param tweet instance of twitter containing the tweet data, including
     * the media id
     * @return an instance of the TwitterResponse containg the tweeted tweet
     */
    public TweetResponse tweet(Tweet tweet) {
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        String tweetURI = this.clientConfiguration.getTweetEndpoint();
        
        try {
            String tweetJson = this.objectMapper.writeValueAsString(tweet);
            
            HttpEntity<String> requestEntity = new HttpEntity<>(tweetJson, headers);
            
            ResponseEntity<TweetResponse> response = this.restTemplate
                    .postForEntity(tweetURI, requestEntity, TweetResponse.class);
            
            return response.getBody();
            
        } catch (JsonProcessingException ex) {
            throw new UnavailableTweetServiceException("Coudln't parse "
                    + tweet + " to json.", ex);
        }
        
    }

    
}
