/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.tweet.client;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class UnavailableTweetServiceException extends RuntimeException {

    public UnavailableTweetServiceException(String message) {
        super(message);
    }

    public UnavailableTweetServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnavailableTweetServiceException(Throwable cause) {
        super(cause);
    }
}
