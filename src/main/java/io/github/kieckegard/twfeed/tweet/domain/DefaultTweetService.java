/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.tweet.domain;

import io.github.kieckegard.twfeed.tweet.client.UnavailableTweetServiceException;
import io.github.kieckegard.twfeed.tweet.client.data.Tweet;
import io.github.kieckegard.twfeed.tweet.client.data.TweetResponse;
import io.github.kieckegard.twfeed.tweet.client.data.MediaResponse;
import io.github.kieckegard.twfeed.tweet.client.TweetClient;
import io.github.kieckegard.twfeed.posts.domain.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Service("default")
public class DefaultTweetService implements TweetService {
    
    private TweetClient tweetClient;

    @Autowired
    public DefaultTweetService(TweetClient tweetClient) {
        this.tweetClient = tweetClient;
    }

    /**
     * Tweets the post in the twitter.
     * Uploads the imageUrl, receives the uploaded media Id
     * , creates a {@link io.github.kieckegard.twfeed.tweet.domain.Tweet}
     * containg the post content and the media Id and then tweets it 
     * in the twitter.
     * @param post 
     * @return the tweet id.
     */
    @Override
    public String tweet(Post post) {
        
        MediaResponse mediaResponse = this.tweetClient.uploadMedia(post.getImageUrl());
        if(!mediaResponse.isSuccess())
            throw new UnavailableTweetServiceException("Coudln't upload the image "
                    + post.getImageUrl());
        
        String mediaId = mediaResponse.getMedia();
        
        Tweet tweet = new Tweet(post.getTitle(), mediaId, post.getSourceUrl());
        
        TweetResponse tweetResponse = this.tweetClient.tweet(tweet);
        
        if(!tweetResponse.isSuccess())
            throw new UnavailableTweetServiceException("Coudln't tweet the post " + post.getId());
        
        return tweetResponse.getTweet();
    }
    
}
