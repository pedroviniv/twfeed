/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.tweet.worker;

import io.github.kieckegard.twfeed.posts.domain.Post;
import io.github.kieckegard.twfeed.publication.domain.PublicationService;

/**
 * A Runnable that publishes an post using the PublicationService.
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class Publisher implements Runnable {
    
    private PublicationService publicationService;
    private Post post;

    public Publisher(PublicationService publicationService, Post post) {
        this.publicationService = publicationService;
        this.post = post;
    }

    @Override
    public void run() {
        this.publicationService.publish(this.post);
    }
    
    
}
