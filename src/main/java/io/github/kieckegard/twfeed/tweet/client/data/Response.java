/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.tweet.client.data;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class Response {
    
    private boolean success;
    private String message;
    private int message_code;

    public Response(boolean success, String message, int message_code) {
        this.success = success;
        this.message = message;
        this.message_code = message_code;
    }

    protected Response() {}

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMessage_code() {
        return message_code;
    }

    public void setMessage_code(int message_code) {
        this.message_code = message_code;
    }

    @Override
    public String toString() {
        return "Response{" + "success=" + success + ", message=" + message + ", message_code=" + message_code + '}';
    }
}
