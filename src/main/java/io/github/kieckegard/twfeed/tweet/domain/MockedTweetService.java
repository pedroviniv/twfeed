/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.tweet.domain;

import io.github.kieckegard.twfeed.tweet.client.TweetClient;
import io.github.kieckegard.twfeed.posts.domain.Post;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@Service("mock")
public class MockedTweetService implements TweetService {

    private static final Logger LOG = Logger.getLogger(MockedTweetService.class.getName());

    private TweetClient tweetClient;

    @Autowired
    public MockedTweetService(TweetClient tweetClient) {
        this.tweetClient = tweetClient;
    }
    
    @Override
    public String tweet(Post post) {
        String tweetId = UUID.randomUUID().toString();
        LOG.log(Level.INFO, "The tweet {0} has been published.", tweetId);
        return tweetId;
    }
    
    public void remove(String tweetId) {
        LOG.log(Level.INFO, "The tweet {0} has been removed.", tweetId);
    }
}
