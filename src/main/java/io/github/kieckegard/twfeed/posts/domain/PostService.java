/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.posts.domain;

import io.github.kieckegard.twfeed.posts.infrastructure.PostRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Service
public class PostService {
    
    private PostRepository postRepository;
    private PostValidator postValidator;

    @Autowired
    public PostService(PostRepository postRepository, PostValidator postValidator) {
        this.postRepository = postRepository;
        this.postValidator = postValidator;
    }
    
    /**
     * Validates and saves the post.
     * The Generated Id is returned
     * @param post the post entity
     * @return Long representing the generated Id
     */
    public Long save(Post post) {
        
        this.postValidator.validate(post);
        this.postRepository.save(post);
        return post.getId();
    }
    
    public Page<Post> paginatedPosts(int page, int itensSize) {
        
        PageRequest pageRequest = new PageRequest(page, itensSize);
        
        Page<Post> postsPage = this.postRepository.findAll(pageRequest);
        
        return postsPage;
    }

    
}
