/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.posts.domain;

import io.github.kieckegard.twfeed.posts.infrastructure.PostRepository;
import javax.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Service
public class PostValidator {
    
    private PostRepository postRepository;

    @Autowired
    public PostValidator(PostRepository postRepository) {
        this.postRepository = postRepository;
    }
    
    public void validate(Post post) {
      
        if(alreadyExists(post))
            throw new ValidationException(ErrorMessages.ALREADY_EXISTS.toErrorMsg());
    }
    
    private boolean alreadyExists(Post post) {
        
        Long count = this.postRepository
                .countByTitleOrSourceUrl(post.getTitle(), post.getSourceUrl());
        return count > 0;
    }
    
}
