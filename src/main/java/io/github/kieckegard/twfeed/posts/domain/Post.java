/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.posts.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Entity
public class Post implements Serializable {
    
    public static final String LIST_NOT_PUBLISHED_POSTS 
            =   "SELECT * " +
                "FROM post p " +
                "WHERE NOT EXISTS( " +
                "   SELECT p.id " +
                "   FROM publication pu " +
                "   WHERE pu.post_id = p.id " +
                ") " +
                "UNION " +
                "SELECT p.* " +
                "FROM post p JOIN publication pu ON p.id = pu.post_id " +
                "GROUP BY p.id " +
                "HAVING COUNT(pu.id) FILTER (WHERE active = true) = 0 " +
                "ORDER BY created_at ASC " +
                "LIMIT :limit";
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String sourceUrl;
    private String imageUrl;
    private String title;
    @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
    private boolean posted;
    
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public Post(String sourceUrl, String imageUrl, String title) {
        this.sourceUrl = sourceUrl;
        this.imageUrl = imageUrl;
        this.title = title;
        this.posted = false;
    }
    
    @PrePersist
    protected void onCreate() {
        this.createdAt = LocalDateTime.now();
    }

    protected Post() {
        this.posted = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isPosted() {
        return posted;
    }

    public void setPosted(boolean posted) {
        this.posted = posted;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Post{" + "id=" + id + ", sourceUrl=" + sourceUrl + ", imageUrl=" + imageUrl + ", title=" + title + ", posted=" + posted + ", createdAt=" + createdAt + '}';
    }
}
