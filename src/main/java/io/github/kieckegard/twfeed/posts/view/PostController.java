/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.posts.view;

import io.github.kieckegard.twfeed.posts.domain.Post;
import io.github.kieckegard.twfeed.posts.domain.PostService;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Controller
public class PostController {
    
    private PostService postService;
    private static final Logger LOG = Logger.getLogger(PostController.class.getName());

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }
    
    @GetMapping("/")
    public ModelAndView index() {
        return new ModelAndView("redirect:/posts");
    }
    
    @PostMapping(value = "/posts")
    public ModelAndView cadastrarPost(Post post, HttpServletResponse response, 
            HttpServletRequest request, RedirectAttributes redirectAttrs) {
        
        try {
            this.postService.save(post);
            response.setStatus(HttpStatus.CREATED.value());
            redirectAttrs.addFlashAttribute("successMsg", "O post " +  post.getTitle() + " foi salvo com sucesso.");
            
            return new ModelAndView("redirect:/posts");
        } catch (ValidationException ve) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return new ModelAndView("/");
        }
    }
    
    @GetMapping(value = "/posts")
    public String listarPosts(Model model, 
            @RequestParam(required = false, name = "page", defaultValue = "0") 
                    int page) {
        
        Page<Post> paginatedPosts = this.postService.paginatedPosts(page, 20);
        model.addAttribute("posts", paginatedPosts);
        
        return "/index";
    }
}
