/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.posts.domain;

import java.util.ResourceBundle;

/**
 * All error messsages can be updated in error-messages.properties file
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
enum ErrorMessages {

    INVALID_TITLE, INVALID_IMAGE, INVALID_URL, ALREADY_EXISTS;

    private final static String prefix = "post.";
    private final static ResourceBundle errorBundle = ResourceBundle.getBundle("error-messages");

    public String toErrorMsg() {
        String errorCode = prefix.concat(this.name());

        String errorMsg = errorBundle.getString(errorCode);

        return errorMsg;
    }

}
