/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.posts.infrastructure;

import io.github.kieckegard.twfeed.posts.domain.Post;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
 
    @Query("SELECT COUNT(p) FROM Post p WHERE p.title = :title OR p.sourceUrl = :sourceUrl")
    public Long countByTitleOrSourceUrl(@Param("title") String title, 
            @Param("sourceUrl") String sourceUrl);
    
    @Query(value = Post.LIST_NOT_PUBLISHED_POSTS, nativeQuery = true)
    public List<Post> getUnpublishedPostsWithLimit(@Param("limit") int limit);
}
