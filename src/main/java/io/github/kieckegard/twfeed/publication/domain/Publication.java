/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.publication.domain;

import io.github.kieckegard.twfeed.posts.domain.Post;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Entity
public class Publication implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    @Column(name="tweet_id", nullable = false)
    private String tweetId;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id")
    private Post post;
    
    @Column(name = "publised_at")
    private LocalDateTime publishedAt;
    
    @Column(name = "persisted_at")
    private LocalDateTime persistedAt;
    
    @Column(columnDefinition = "BOOLEAN DEFAULT TRUE")
    private boolean active;

    public Publication(Post post, String tweetId, LocalDateTime publishedAt) {
        this.post = post;
        this.tweetId = tweetId;
        this.publishedAt = publishedAt;
        this.active = true;
    }
    
    @PrePersist
    protected void onCreate() {
        this.persistedAt = LocalDateTime.now();
    }
    
    protected Publication() {
        this.active = true;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public LocalDateTime getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(LocalDateTime publishedAt) {
        this.publishedAt = publishedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getPersistedAt() {
        return persistedAt;
    }

    public void setPersisted_at(LocalDateTime persistedAt) {
        this.persistedAt = persistedAt;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getTweetId() {
        return tweetId;
    }

    public void setTweetId(String tweetId) {
        this.tweetId = tweetId;
    }

    @Override
    public String toString() {
        return "Publication{" + "id=" + id + ", tweetId=" + tweetId + ", post=" + post + ", publishedAt=" + publishedAt + ", persisted_at=" + persistedAt + ", active=" + active + '}';
    }
}
