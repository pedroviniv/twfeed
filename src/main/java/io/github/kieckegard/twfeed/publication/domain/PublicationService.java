/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.publication.domain;

import io.github.kieckegard.twfeed.posts.domain.Post;
import io.github.kieckegard.twfeed.tweet.domain.MockedTweetService;
import io.github.kieckegard.twfeed.tweet.client.UnavailableTweetServiceException;
import io.github.kieckegard.twfeed.tweet.domain.TweetService;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Service
public class PublicationService {
    
    private PublicationRepository publicationRepository;
    private TweetService tweetService;
    private static final Logger LOG = Logger.getLogger(PublicationService.class.getName());

    @Autowired
    public PublicationService(
            PublicationRepository publicationRepository, 
            @Qualifier("default") TweetService tweetService) {
        this.publicationRepository = publicationRepository;
        this.tweetService = tweetService;
    }
    
    /**
     * Verifies if an post (by the postId) was already published.
     * @param postId the post primary key
     * @return an boolean value indicating if the post was published
     */
    public boolean hasPublished(Long postId) {
        return this.publicationRepository
                .countByPostIdAndActive(postId, true) > 0;
    }
    
    /**
     * Publishes an post in the twitter.
     * It uses TweetService to tweets this post in the twitter.
     * If the tweet proccess went fine, a tweetId is returned
     * and a Publication is created and saved into the database.
     * A Publication is saved in due to keep track of what posts
     * was published in the twitter. The Publication is saved with the 
     * following data:
     * <ul>
     *  <li>post_id (Long)</li>
     *  <li>tweet_id (String)</li>
     *  <li>published_at (LocalDate)</li>
     *  <li>created_at (LocalDate)</li>
     *  <li>active (boolean)</li>
     * </ul>
     * @param post post to be published
     * @return the created Publication
     */
    public Publication publish(Post post) {
        
        boolean alreadyPublished = this.hasPublished(post.getId());
        
        
        if(alreadyPublished) {
            throw new ValidationException("This post has already been published.");
        }
        try {
            
            String tweetId = this.tweetService.tweet(post);
            
            //retrieve from tweetService
            LocalDateTime publishedAt = LocalDateTime.now();

            Publication publication = new Publication(post, tweetId, publishedAt);
            this.publicationRepository.save(publication);
            
            return publication;
            
        } catch (UnavailableTweetServiceException ex) {
            LOG.log(Level.WARNING, "TweetService is unavailable. throwable: ", ex);
            return null;
        }
    }
}
