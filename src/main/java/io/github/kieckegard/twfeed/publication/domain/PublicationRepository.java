/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.twfeed.publication.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public interface PublicationRepository extends JpaRepository<Publication, Long> {
    
    @Query("SELECT p FROM Publication p WHERE p.post.id = :postId AND p.active = :active")
    public Long countByPostIdAndActive(@Param("postId") Long postId, 
            @Param("active") boolean active);
}
